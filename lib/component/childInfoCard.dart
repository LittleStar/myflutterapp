import 'package:flutter/material.dart';

Widget placePhoto(BuildContext context) {
  return Stack(
    alignment: const Alignment(-0.7, -0.9),
    children: [
      buildCard(context),
      childAvatar,
    ],
  );
}

Widget childAvatar = Container(
  padding: EdgeInsets.all(20),
  decoration: BoxDecoration(
    color: const Color.fromARGB(255, 252, 144, 5),
    borderRadius: const BorderRadius.all(const Radius.circular(8)),
  ),
  child: Image.asset('assets/images/user_img_boy@3x.png',
      width: 88.0, height: 88.0),
);

Widget buildCard(BuildContext context) {
  return Container(
      margin: EdgeInsets.fromLTRB(20, 40, 20, 20),
      padding: EdgeInsets.all(20),
      height: 210,
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(width: 1, color: Colors.black12),
          borderRadius: const BorderRadius.all(const Radius.circular(8)),
          boxShadow: <BoxShadow>[
            new BoxShadow(
                color: Colors.black12,
//            offset: new Offset(0.0, 6.0),
                blurRadius: 10.0,
                spreadRadius: 3.0)
          ]),
      child: Container(
        margin: EdgeInsets.only(top: 44),
        child: Column(
          children: <Widget>[
            rightInfo,
            Divider(color: const Color(0XFFF0F0F0)),
            bottomInfo(context),
          ],
        ),
      ));
}

Widget rightInfo = DefaultTextStyle.merge(
    style: descTextStyle,
    child: Container(
        margin: EdgeInsets.only(bottom: 18),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text('Amy', style: TextStyle(fontSize: 24)),
            Container(
                padding: EdgeInsets.only(left: 10.0, right: 10.0),
                child: (Image.asset('assets/images/icon_boy@3x.png',
                    fit: BoxFit.fill, width: 20.0, height: 20.0))),
            Text('5y2m', style: TextStyle(fontSize: 16)),
          ],
        )));

final descTextStyle = TextStyle(
  color: const Color(0XFF3c4858),
  fontWeight: FontWeight.w700,
);

Widget bottomInfo(BuildContext context) {
  return Container(
      margin: EdgeInsets.only(top: 10),
      child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Text('Make a plan to change now!'),
      RaisedButton(
          child: Center(
              child: Text('Make a plan',
                  style: TextStyle(
                    color: Colors.white,
                  ))),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(30.0)),
          onPressed: () {}),
    ],
  ));
}

Widget button(BuildContext context) {
  return Container(
      width: 100,
      height: 30,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(const Radius.circular(15)),
        color: Theme.of(context).accentColor,
      ),
      child: Center(
          child: Text('Make a plan',
              style: TextStyle(
                color: Colors.white,
              ))));
}
