import 'package:flutter/material.dart';
import 'component/childInfoCard.dart';
import 'component/test.dart';
//import 'package:flutter/rendering.dart';

void main() {
//  debugPaintSizeEnabled = true;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Home'),
        ),
        body: Container(child: placePhoto(context)),
      ),
      theme: ThemeData(
        // TODO: define needed
          primarySwatch: Colors.teal,
          primaryColor: Color(0xFF55B3B7),
          accentColor: Color(0xFF55B3B7),
          primaryColorBrightness: Brightness.dark,
          accentColorBrightness: Brightness.dark,
          scaffoldBackgroundColor: Color(0xFFF6F6F6),
          buttonColor: Color(0xFF55B3B7),
          inputDecorationTheme:
          InputDecorationTheme(border: OutlineInputBorder())),
    );
  }
}

//import 'package:flutter/foundation.dart';
//import 'package:flutter/material.dart';
//
//void main() {
//  runApp(MyApp());
//}
//
//class MyApp extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    final appName = 'Custom Themes';
//
//    return MaterialApp(
//      title: appName,
//      theme: ThemeData(
//        // Define the default Brightness and Colors
//        brightness: Brightness.dark,
//        primaryColor: Colors.lightBlue[800],
//        accentColor: Colors.green,
//        buttonColor: Colors.cyan,
//
//        // Define the default Font Family
//        fontFamily: 'Montserrat',
//
//        // Define the default TextTheme. Use this to specify the default
//        // text styling for headlines, titles, bodies of text, and more.
//        textTheme: TextTheme(
//          headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
//          title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
//          body1: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
//        ),
//      ),
//      home: MyHomePage(
//        title: appName,
//      ),
//    );
//  }
//}
//
//class MyHomePage extends StatelessWidget {
//  final String title;
//
//  MyHomePage({Key key, @required this.title}) : super(key: key);
//
//  _log() {
//    print("点击了按钮");
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: Text(title),
//      ),
//      body: Center(
////          color: Theme.of(context).accentColor,
//          child: new RaisedButton(
////              color: Colors.blue,
//              onPressed: _log,
//              child: new Text("Press Me11111"),
//              shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0))
//          ),
//      ),
//
////      floatingActionButton: Theme(
////        data: ThemeData(accentColor: Colors.yellowAccent),
////        child: FloatingActionButton(
////          onPressed: null,
////          child: Icon(Icons.add),
////        ),
////      ),
//
//    );
//  }
//}

